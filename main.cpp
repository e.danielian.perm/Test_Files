#include <iostream>
#include <string>
#include <fstream>
using namespace std;

typedef struct{
    string first_name;
    string last_name;
    unsigned short age;
} Student;

int main() {
    //write in file
    int n;
    cout<<"Input number of students: ";
    cin>>n;
    cout<<"Format: First name *enter*\nLast name *enter*\nAge *enter*\n";
    Student* a;
    a=new Student[n];
    for(int i=0; i<n; i++)
        cin>>a[i].first_name>>a[i].last_name>>a[i].age;
    ofstream fout("student.txt");
    fout<<n<<endl;//in first line number of students
    for(int i=0; i<n; i++)
        fout<<a[i].first_name<<' '<<a[i].last_name<<' '<<a[i].age<<endl;
    fout.close();
    //read from file
    ifstream fin("student.txt");
    int m;
    fin>>m;
    Student*b;
    b=new Student[m];
    for(int i=0; i<m; i++)
        fin>>b[i].first_name>>b[i].last_name>>b[i].age;
    cout<<"Students:"<<endl;
    for(int i=0; i<m; i++)
        cout<<b[i].first_name<<' '<<b[i].last_name<<' '<<b[i].age<<endl;
    fin.close();
    return 0;
}